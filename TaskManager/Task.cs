﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager
{
    public class Task
    {
        public string Titel { get; set; }

        public int TaskId { get; set; }

        private bool gennemført = false;

        public bool Gennemført
        {
            get { return gennemført; }
            set { gennemført = value; }
        }


        public Task(int taskId, string titel, bool gennemført)
        {
            this.TaskId = taskId;
            this.Titel = titel;
            this.Gennemført = gennemført;
        }

        public Task()
        {

        }

       
        public override string ToString()
        {
            return $"{TaskId}. {Titel}";
        }









    }
}
