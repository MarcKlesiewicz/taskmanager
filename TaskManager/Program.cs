﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager
{

    
    class Program
    {
        public static void Main(string[] args)
        {

            List<Task> tasks = new List<Task>();
            CitatMaskine citatMaskine = new CitatMaskine();
            Level level = new Level(1);
            
            int taskCount = 1;
            int coins = 0;
            int taskCompleted = 0;
   

            while (true)
            {

                Console.Clear();

                PrintLogo();
                level.CheckLevel(taskCompleted);

                Console.WriteLine($"                                                                                                         Level {level.LevelNum}: {level.LevelStatus}     ");
                Console.WriteLine($"                                                                                                         {coins} X COINS     ");




                foreach (var item in tasks)
                {
                    if (item.Gennemført)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(item.ToString());
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine(item.ToString());
                    }

                }

                int input = 0;
                try
                {

                   input = int.Parse(Console.ReadLine());

                }
                catch (Exception)
                {

                    Console.WriteLine("Du indtastede et ugyldigt input...");
                    Console.ReadKey();
                }

                switch (input)
                {
                    case 1:

                        Console.WriteLine("\nSkriv din task her: ");
                        string taskTekst = Console.ReadLine();

                        Task tempTask = new Task(taskCount, taskTekst, false);
                        tasks.Add(tempTask);

                        taskCount++;
                        break;


                    case 2:

                        try
                        {
                            Console.WriteLine("\nIndtast task'en du ønsker at tjekke af: ");
                            int taskNum = int.Parse(Console.ReadLine());

                            foreach (var item in tasks)
                            {
                                if (item.TaskId == taskNum)
                                {

                                    Task task = (Task)item;
                                    item.Gennemført = true;
                                }
                            }
                            coins++;
                            taskCompleted++;
                        }
                        catch (Exception)
                        {

                            Console.WriteLine("Input eksitere ikke");
                            Console.ReadKey();
                        }
                       
                        break;

                    case 3:

                        tasks.Clear();
                        break;

                    case 4:

                        if (coins >= 1)
                        {
                            Console.WriteLine("\n" + citatMaskine.Generater());
                            coins--;
                        }
                        else
                        {
                            Console.WriteLine("INGEN COINS, INGEN QUOTES \n(optjen coins ved at gennemføre tasks din slagger)");
                        }
                        
                        Console.ReadKey();
                        break;

                }

            }

        }

        private static void PrintLogo()
        {
            Console.WriteLine(@"                                                                                                                  ");
            Console.WriteLine(@"  _______        _____ _  ____  __          _   _          _____ ______ _____             ____   ___   ___   ___  ");
            Console.WriteLine(@" |__   __|/\    / ____| |/ /  \/  |   /\   | \ | |   /\   / ____|  ____|  __ \           |___ \ / _ \ / _ \ / _ \ ");
            Console.WriteLine(@"    | |  /  \  | (___ | ' /| \  / |  /  \  |  \| |  /  \ | |  __| |__  | |__) |  ______    __) | | | | | | | | | |");
            Console.WriteLine(@"    | | / /\ \  \___ \|  < | |\/| | / /\ \ | . ` | / /\ \| | |_ |  __| |  _  /  |______|  |__ <| | | | | | | | | |");
            Console.WriteLine(@"    | |/ ____ \ ____) | . \| |  | |/ ____ \| |\  |/ ____ \ |__| | |____| | \ \            ___) | |_| | |_| | |_| |");
            Console.WriteLine(@"    |_/_/    \_\_____/|_|\_\_|  |_/_/    \_\_| \_/_/    \_\_____|______|_|  \_\          |____/ \___/ \___/ \___/ ");
            Console.WriteLine(@"                                                                                                                  ");
            Console.WriteLine(@"                                                                                                                  ");
            Console.WriteLine(@"    [1] Tilføj en task | [2] Marker task som gennemført | [3] Ryd liste | [4] Dit daglige random-genrated quote. ");
            Console.WriteLine(@"                                                                                                                  ");
        }

        

    }
}


