﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TaskManager
{
    public class CitatMaskine
    {

        Random random = new Random();
        public CitatMaskine()
        {

        }

        public string Generater()
        {

            int ranNum = random.Next(1,9);
            string citat = "";
            try
            {
                using (StreamReader inputFile = new StreamReader("Citater.txt"))
                {
                    
                    for (int i = 1; i < 10; i++)
                    {
                       citat = inputFile.ReadLine();

                        if (i == ranNum)
                        {
                            return citat;
                        }
                   
                    }

                    inputFile.Close();
                                    
                }
            }
            catch (IOException e)
            {
                Console.WriteLine($"Kunne ikke finde filen: ");
                Console.WriteLine(e.Message);
            }
            

            return citat;
        }
    }
}
