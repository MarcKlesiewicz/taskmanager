﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager
{

    public enum Levels
    {
        lazyBoi,
        greatDude,
        madLad,
        
    }
    public class Level
    {

        public Levels LevelStatus { get; set; }
        public int LevelNum { get; set; }

        public Level(int levelNum)
        {
            this.LevelNum = levelNum;
        }

        public void CheckLevel(int completedTask)
        {

            if (completedTask < 5)
            {
                LevelStatus = Levels.lazyBoi;
               
                
            }
            else if (completedTask >= 5 && completedTask < 10)
            {
                LevelStatus = Levels.greatDude;
                LevelNum = 2;
            }
            else if (completedTask >= 10)
            {
                LevelStatus = Levels.madLad;
                LevelNum = 2;
            }
            
            

            
        }
    }
}

